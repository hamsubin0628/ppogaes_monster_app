import 'package:flutter/material.dart';
import 'package:ppogaes_monster_app/model/duel_item.dart';

class ComponentMonsterDuelItem extends StatefulWidget {
  const ComponentMonsterDuelItem({
    super.key,
    required this.duelItem,
    required this.callback,
    required this.isMuTurn,
    required this.isLive,
    required this.currentHp
  });
  
  final DuelItem duelItem;
  final VoidCallback callback;
  final bool isMuTurn;
  final bool isLive;
  final num currentHp;

  @override
  State<ComponentMonsterDuelItem> createState() => _ComponentMonsterDuelItemState();
}

class _ComponentMonsterDuelItemState extends State<ComponentMonsterDuelItem> {
  double _calculateHpPercent(){
    return widget.currentHp / widget.duelItem.physical;
  }

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return Container(
      child: Column(
        children: [
          Container(
            child: Row(
              children: [
                Container(
                  child: Column(
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 3,
                        height: MediaQuery.of(context).size.width / 3,
                        child: Image.asset(
                          '${widget.isLive ? widget.duelItem.imgUrl : 'assets/images/ghost.png'}',
                          fit: BoxFit.fill,
                        ),
                      ),
                      Text(
                        widget.duelItem.name,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            letterSpacing: -1.0,
                            color: Color(0xffce2121)
                        ),
                      ),
                      Text(
                        '총 HP : ${widget.duelItem.physical}',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.black87.withOpacity(0.7)
                        ),
                      ),
                      Text(
                        '공격력 : ${widget.duelItem.power}',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.black87.withOpacity(0.7)
                        ),
                      ),
                      Text(
                        '방어력 : ${widget.duelItem.defensive}',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.black87.withOpacity(0.7)
                        ),
                      ),
                      Text(
                        '스피드 : ${widget.duelItem.speed}',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.black87.withOpacity(0.7)
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: Column(
                          children: [
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 2.3,
                              child: LinearProgressIndicator(
                                value: _calculateHpPercent(),
                                color: Color(0xffce2121),
                                backgroundColor: Colors.grey.withOpacity(0.3),
                                borderRadius: BorderRadius.circular(5),
                                minHeight: 5,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: Column(
                          children: [
                            (widget.isMuTurn && widget.isLive) ?
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Color(0xffce2121),
                                minimumSize: const Size(70, 45),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6)
                                ),
                                elevation: 10.0,
                              ),
                              onPressed: widget.callback,
                              child: Text(
                                  '공격',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: -1.0,
                                  color: Colors.white
                                ),
                              ),
                            ) : Container(),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
