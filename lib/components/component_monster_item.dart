import 'package:flutter/material.dart';
import 'package:ppogaes_monster_app/model/monster_item.dart';

class ComponentMonsterItem extends StatelessWidget {
  const ComponentMonsterItem({
    super.key,
    required this.monsterItem,
    required this.callback
  });

  final MonsterItem monsterItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              width: 250, height: 330,
              margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black12.withOpacity(0.06),
                      spreadRadius: 10,
                      blurRadius: 25,
                      offset: Offset(0,0)
                  )
                ],
              ),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: Image.asset(
                      monsterItem.imgUrl,
                      width: 140,
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                          child: Text(
                              monsterItem.name,
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              letterSpacing: -1.0,
                              color: Color(0xffce2121),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 20, 0, 0),
                          child: Text(
                            '속성 : ${monsterItem.attack}',
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                              letterSpacing: -1.0,
                              color: Colors.black87.withOpacity(0.5),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 20, 0, 0),
                          child: Text(
                            '〉',
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              letterSpacing: -1.0,
                              color: Colors.black87.withOpacity(0.5),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 7, 0, 0),
                    width: 192, height: 1,
                    color: Colors.grey.withOpacity(0.3),
                  ),
                  Container(
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.fromLTRB(20, 8, 0, 0),
                    child: Text(
                      '전투력 : ${monsterItem.power}',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        letterSpacing: -1.0,
                        color: Colors.black87.withOpacity(0.5),
                      ),
                    ),
                  ),
                  Container(
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Text(
                      '체력 : ${monsterItem.physical}HP',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        letterSpacing: -1.0,
                        color: Colors.black87.withOpacity(0.5),
                      ),
                    ),
                  ),
                  Container(
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Text(
                      '방어력 : ${monsterItem.defensive}',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        letterSpacing: -1.0,
                        color: Colors.black87.withOpacity(0.5),
                      ),
                    ),
                  ),
                  Container(
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Text(
                      '스피드 : ${monsterItem.speed}',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        letterSpacing: -1.0,
                        color: Colors.black87.withOpacity(0.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
