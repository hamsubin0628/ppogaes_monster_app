import 'package:flutter/material.dart';
import 'package:ppogaes_monster_app/pages/page_duel_stage.dart';
import 'package:ppogaes_monster_app/pages/page_monster_list.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
          fontFamily: "GmarketSansTTF"
      ),
      home: PageDuelStage(),
    );
  }
}