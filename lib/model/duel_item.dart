class DuelItem {
  String imgUrl;
  String name;
  num power;
  num physical;
  num defensive;
  num speed;

  DuelItem(this.imgUrl, this.name, this.power, this.physical, this.defensive, this.speed);
}