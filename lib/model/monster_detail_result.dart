import 'package:ppogaes_monster_app/model/monster_response.dart';

class MonsterDetailResult {
  String msg;
  num code;
  MonsterResponse data;

  MonsterDetailResult(this.msg, this.code, this.data);

  factory MonsterDetailResult.fromJson(Map<String, dynamic> json) {
    return MonsterDetailResult(
        json['msg'],
        json['code'],
        MonsterResponse.fromJson(json['data'])
    );
  }
}