class MonsterItem {
  num id;
  String imgUrl;
  String name;
  String attack;
  num power;
  num physical;
  num defensive;
  num speed;
  String classify;
  String property;

  MonsterItem(this.id, this.imgUrl, this.name, this.attack, this.power, this.physical, this.defensive, this.speed ,this.classify, this.property);

  factory MonsterItem.fromJson(Map<String, dynamic> json){
    return MonsterItem(
        json['id'],
        json['imgUrl'],
        json['name'],
        json['attack'],
        json['power'],
        json['physical'],
        json['defensive'],
        json['speed'],
        json['classify'],
        json['property']
    );
  }
}