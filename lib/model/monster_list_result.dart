import 'package:ppogaes_monster_app/model/monster_item.dart';

class MonsterListResult {
  String msg;
  num code;
  List<MonsterItem> list;
  num totalCount;

  MonsterListResult(this.msg, this.code, this.list, this.totalCount);

  factory MonsterListResult.fromJson(Map<String, dynamic> json) {
    return MonsterListResult(
        json['msg'],
        json['code'],
        json['list'] !=null ?
            (json['list'] as List).map((e) => MonsterItem.fromJson(e)).toList()
            : [],
        json['totalCount']
    );
    // json으로 [{name :'ghd'}, {name:'rla'}]이런 string을 받아서 Object의 List로 바꾸고 난 후에
    // 그럼 어쩃든 List니까 한 뭉탱이씩 던져줄 수 있다.
    // 근데.. 한뭉댕이씩 던지겠다 하면서 한 뭉탱이 부르는거의 이름이 바로 e
    // 다 작은 그릇으로 바꿔치기 한 다음에
    // 다시 그것들을 싹 다져와서 리스트로 촥 하고 정리해줌.
  }
}