class MonsterResponse {
  num id;
  String imgUrl;
  String name;
  String attack;
  num power;
  num physical;
  num defensive;
  num speed;
  String gender;
  String classify;
  num stature;
  num weight;
  String property;
  String etcMemo;

  MonsterResponse(this.id,
      this.imgUrl,
      this.name,
      this.attack,
      this.power,
      this.physical,
      this.defensive,
      this.speed,
      this.gender,
      this.classify,
      this.stature,
      this.weight,
      this.property,
      this.etcMemo
      );

  factory MonsterResponse.fromJson(Map<String, dynamic> json) {
    return MonsterResponse(
        json['id'],
        json['imgUrl'],
        json['name'],
        json['attack'],
        json['power'],
        json['physical'],
        json['defensive'],
        json['speed'],
        json['gender'],
        json['classify'],
        json['stature'],
        json['weight'],
        json['property'],
        json['etcMemo']
    );
  }
}