import 'package:flutter/material.dart';
import 'package:ppogaes_monster_app/components/component_monster_duel_item.dart';
import 'package:ppogaes_monster_app/pages/page_duel_stage.dart';
import 'package:ppogaes_monster_app/pages/page_monster_list.dart';

class PageDuel extends StatefulWidget {
  const PageDuel({
    super.key,
  });

  @override
  State<PageDuel> createState() => _PageDuelState();
}

class _PageDuelState extends State<PageDuel> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xffce2121),
        foregroundColor: Colors.white,
        shadowColor: Colors.black,
        title: IconButton(
          icon: Image.asset(
            'assets/images/pokemonlogo.png',
            height: 45,
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageMonsterList()));
          },
        ),
      ),
        body: _buildBody(context)
    );
  }
}

Widget _buildBody(BuildContext context) {
  double phoneWidth = MediaQuery.of(context).size.width;

  return SingleChildScrollView(
    child: Column(
      children: [
        Container(
          width: phoneWidth,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  colorFilter: ColorFilter.mode(
                      Colors.white.withOpacity(0.3),BlendMode.dstATop
                  ),
                  image: AssetImage(
                    'assets/images/duel_bg.png',
                  ),
                  fit: BoxFit.fill
              )
          ),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                width: phoneWidth - 40,
                height: 60,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12.withOpacity(0.06),
                        spreadRadius: 10,
                        blurRadius: 25,
                        offset: Offset(0,0)
                    )
                  ],
                ),
                child: Text(
                  'Duel Stage',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 25,
                      letterSpacing: -1.0,
                      color: Color(0xffce2121),
                      height: 2.4
                  ),
                ),
              ),
              Container(
                alignment: FractionalOffset.centerRight,
                padding: EdgeInsets.fromLTRB(0, 20, 20, 0),
                child:  ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xffffd628),
                    minimumSize: const Size(70, 55),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)
                    ),
                    elevation: 10.0,
                  ),
                  onPressed: () {},
                  child: Text(
                    '변경하기',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      letterSpacing: -1.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: FractionalOffset.centerRight,
                margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                child: Image.asset(
                  'assets/images/1_Pikachu.png',
                  width: phoneWidth / 2,
                ),
              ),
              Container(
                child: Text(
                    'VS',
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.w600,
                    color:  Color(0xffce2121),
                    letterSpacing: -1.0
                  ),
                ),
              ),
              Container(
                alignment: FractionalOffset.centerLeft,
                padding: EdgeInsets.fromLTRB(20, 10, 0, 0),
                child:  ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xffffd628),
                    minimumSize: const Size(70, 55),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)
                    ),
                    elevation: 10.0,
                  ),
                  onPressed: () {},
                  child: Text(
                    '변경하기',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      letterSpacing: -1.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                alignment: FractionalOffset.centerLeft,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset(
                  'assets/images/5_Charmander.png',
                  width: phoneWidth / 2,
                ),
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Column(
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Color(0xffce2121),
                          minimumSize: const Size(460, 70),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)
                          ),
                          elevation: 10.0,
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageDuelStage()));
                        },
                        child: Text(
                          '배틀 시작하기',
                          style: TextStyle(
                            fontSize: 23,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
              )
            ],
          ),
        ),
      ],
    ),
  );
}