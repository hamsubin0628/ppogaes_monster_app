import 'package:flutter/material.dart';
import 'package:ppogaes_monster_app/components/component_monster_duel_item.dart';
import 'package:ppogaes_monster_app/model/duel_item.dart';
import 'package:ppogaes_monster_app/pages/page_monster_list.dart';

class PageDuelStage extends StatefulWidget {
  const PageDuelStage({super.key});

  @override
  State<PageDuelStage> createState() => _PageDuelStageState();
}

class _PageDuelStageState extends State<PageDuelStage> {
  DuelItem monster1 = DuelItem('assets/images/1_Pikachu.png', '피카츄', 20000, 10000, 5000, 6000);
  DuelItem monster2 = DuelItem('assets/images/5_Charmander.png', '파이리', 10000, 5000, 1000, 5000);

  bool _isTurnMonster1 = true; // 기본으로 1번 몬스터 선빵, 1번 몬스터 순서면 2번 몬스터 순서 아님.

  bool _monster1Live = true;
  num _monster1CurrentHp = 0;

  bool _monster2Live = true;
  num _monster2CurrentHp = 0;

  String _gameLog = '';

  @override
  void initState() {
    super.initState();
    _calculateFirstTurn(); // 몬스터 선빵 판별
    setState(() {
      _monster1CurrentHp = monster1.power; // 1번 몬스커 잔여 hp ( 페이지 들어 가자마자니까 최대최력만큼 )
      _monster2CurrentHp = monster2.power; // 1번 몬스커 잔여 hp ( 페이지 들어 가자마자니까 최대최력만큼 )
    });
  }

  //선빵 누군지 계산
  void _calculateFirstTurn() {
    if (monster1.speed < monster2.speed) { // 몬스터 1번보다 몬스터 2번의 스피드가 높으면
      setState(() {
        _isTurnMonster1 = false; // 몬스터 2번 선빵
      });
    }
  }

  // 때렸을때 최종 공격력 몇인지 ( 크리티컬 계산식 넣기 )
  num _calculateResultHitPoint(num myHitPower, num targetDefPower) {
    List<num> criticalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // 10% 확률로 크리티컬 걸리게 할 것이다.
    criticalArr.shuffle(); // 섞는다.

    bool isCritical = false; // 기본으로 크리티컬 안터진다.
    if (criticalArr[0] == 1) isCritical = true; // 다 섞은 criticalArr에 0번쨰 요소가 1이면 크리뜬걸로 한다.

    num resultHit = myHitPower; // 공격력
    if (isCritical) resultHit = resultHit * 2; // 크리티컬이면 공격력 두배 뻥튀기
    resultHit = resultHit - targetDefPower; // 상대방 방어력만큼 데미지 까주기
    resultHit = resultHit.round(); // 반올림 처리

    if(resultHit <= 0) resultHit = 1; // 계산결과 공격이 0이거나 음수일경우 공격포인트 고정1 처리

    return resultHit;
  }

  // 상대 몬스터가 죽었는지 확인하기
  void _checkIsDead(num targetMonster) {
    if (targetMonster == 1 && (_monster1CurrentHp <= 0)) {
      setState(() {
        _monster1Live = false;
      });
    } else if (targetMonster == 2 && (_monster2CurrentHp) <= 0){
      setState(() {
        _monster2Live = false;
      });
    }
  }

  // 공격 처리
  void _attMonster(num actionMonster) {
    num myHitPower = monster1.power;
    num targetDefPower = monster2.power;

    if (actionMonster == 2) {
      myHitPower = monster2.power;
      targetDefPower = monster1.power;
    }

    num resultHit = _calculateResultHitPoint(myHitPower, targetDefPower); // 상대 Hp 몇 까야하는지 계산

    if (actionMonster == 1) { // 공격하는 몬스터가 1번이면 ( 공격당하는 몬스터가 2번 )
      setState(() {
        _monster2CurrentHp -= resultHit; // 2번 몬스터 체력 깎기
        if (_monster2CurrentHp <= 0) _monster2CurrentHp = 0; // 체력 0 미만이면 0으로 고정
        _checkIsDead(2); // 2번 몬스터 죽었는지 확인
      });
    } else {
      setState(() {
        _monster1CurrentHp -= resultHit; // 1번 몬스터 체력 깎기
        if (_monster1CurrentHp <= 0) _monster1CurrentHp = 0; // 체력 0 미만이면 0으로 고정
        _checkIsDead(1); // 1번 몬스터 죽었는지 확인
      });
    }

    setState(() {
      _isTurnMonster1 = !_isTurnMonster1; // 턴 넘기기. not 연산자로 바꿔주기
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xffce2121),
        foregroundColor: Colors.white,
        shadowColor: Colors.black,
        title: IconButton(
          icon: Image.asset(
            'assets/images/pokemonlogo.png',
            height: 45,
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageMonsterList()));
          },
        ),
      ),
      body: _buildBody(context),
    );
  }
  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    colorFilter: ColorFilter.mode(
                        Colors.white.withOpacity(0.3),BlendMode.dstATop
                    ),
                    image: AssetImage(
                      'assets/images/duel_bg.png',
                    ),
                    fit: BoxFit.fill
                )
            ),
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                      ),
                      Container(
                        alignment: FractionalOffset.centerRight,
                        margin: EdgeInsets.fromLTRB(10, 25, 0, 0),
                        child: ComponentMonsterDuelItem(
                            duelItem: monster1,
                            callback: () {
                              _attMonster(1);
                            },
                            isMuTurn:
                            _isTurnMonster1,
                            isLive: _monster1Live,
                            currentHp: _monster1CurrentHp,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Text(
                    'VS',
                    style: TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.w600,
                        color:  Color(0xffce2121),
                        letterSpacing: -1.0
                    ),
                  ),
                ),
                Container(
                  alignment: FractionalOffset.centerLeft,
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  child: ComponentMonsterDuelItem(
                      duelItem: monster2,
                      callback: () {
                        _attMonster(2);
                      },
                      isMuTurn: !_isTurnMonster1,
                      isLive: _monster2Live,
                      currentHp: _monster2CurrentHp,
                  ),
                )
              ],
            ),
          ),
          Text(_gameLog),
        ],
      ),
    );
  }
}
