import 'package:flutter/material.dart';
import 'package:ppogaes_monster_app/model/monster_response.dart';
import 'package:ppogaes_monster_app/pages/page_duel.dart';
import 'package:ppogaes_monster_app/pages/page_monster_list.dart';
import 'package:ppogaes_monster_app/repository/repo_monster.dart';

class PageMonsterDetail extends StatefulWidget {
  const PageMonsterDetail({
    super.key,
    required this.id
  });

  final num id;

  @override
  State<PageMonsterDetail> createState() => _PageMonsterDetailState();
}

class _PageMonsterDetailState extends State<PageMonsterDetail> {
  MonsterResponse? _detail;

  Future<void> _loadDetail() async {
    await RepoMonster().getMonster(widget.id)
        .then((res) => {
          setState(() {
            _detail = res.data;
          })
    });
  }

  void initState(){
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xffce2121),
        foregroundColor: Colors.white,
        shadowColor: Colors.black,
        title: IconButton(
          icon: Image.asset(
            'assets/images/pokemonlogo.png',
            height: 45,
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageMonsterList()));
          },
        ),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context){
    if (_detail == null) {
      return Text('데이터로딩중..');
    } else {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: 500,
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  width: 450, height: 450,
                  margin: EdgeInsets.fromLTRB(10, 25, 10, 0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12.withOpacity(0.06),
                          spreadRadius: 10,
                          blurRadius: 25,
                          offset: Offset(0,0)
                      )
                    ],
                  ),
                  child: Column(
                    children: [
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                        child: Text(
                          'No. ${widget.id}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.5),
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          width: 350,
                          child: Image.asset(_detail!.imgUrl),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                        child: Text(
                          _detail!.name,
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            letterSpacing: -1.0,
                            color: Color(0xffce2121),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                        child: Text(
                          '속성 : ${_detail!.attack}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  width: 450, height: 1,
                  color: Colors.grey.withOpacity(0.3),
                ),
                Container(
                  child: Column(
                    children: [
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 15, 0, 0),
                        child: Text(
                          '전투력 : ${_detail!.power}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: Column(
                          children: [
                            Text(
                              '체력 : ${_detail!.physical}HP',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w500,
                                letterSpacing: -1.0,
                                color: Colors.grey.withOpacity(0.9),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: Text(
                          '방어력 : ${_detail!.defensive}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: Text(
                          '스피드 : ${_detail!.speed}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: Text(
                          '성별 : ${_detail!.gender}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: Text(
                          '특성 : ${_detail!.classify}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: Text(
                          '키 : ${_detail!.stature}m',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: Text(
                          '몸무게 : ${_detail!.weight}kg',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                        child: Text(
                          '특성 : ${_detail!.property}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(30, 0, 30, 20),
                        child: Text(
                          '${_detail!.etcMemo}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            letterSpacing: -1.0,
                            color: Colors.grey.withOpacity(0.9),
                          ),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 35),
                          child: Column(
                            children: [
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Color(0xffce2121),
                                  minimumSize: const Size(452, 70),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  elevation: 10.0,
                                ),
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageDuel()));
                                },
                                child: Text(
                                  '결투장 입장하기',
                                  style: TextStyle(
                                    fontSize: 23,
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: -1.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          )
                      )
                    ],
                  ),
                )
              ],
            ),
          )
          ],
        ),
      );
    }
  }
}
