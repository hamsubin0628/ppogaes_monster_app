import 'package:flutter/material.dart';
import 'package:ppogaes_monster_app/components/component_monster_item.dart';
import 'package:ppogaes_monster_app/model/monster_item.dart';
import 'package:ppogaes_monster_app/pages/page_monster_detail.dart';
import 'package:ppogaes_monster_app/repository/repo_monster.dart';

class PageMonsterList extends StatefulWidget {
  const PageMonsterList({super.key});

  @override
  State<PageMonsterList> createState() => _PageMonsterListState();
}

SliverGridDelegate _sliverGridDelegate(){
  return const SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
      mainAxisExtent: 350,
      mainAxisSpacing: 1
  );
}

class _PageMonsterListState extends State<PageMonsterList> {
  List<MonsterItem> _list = [];

  Future<void> _loadList() async {
    // 이 메서드가 repo 호출해서 데이터 받아온 다음에..
    // setstate해서 _list 교체 할거다....
    await RepoMonster().getMonsters()
        .then((res) => {
          setState(() {
            _list = res.list;
      })
    })
    .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xffce2121),
        foregroundColor: Colors.white,
        shadowColor: Colors.black,
        title: IconButton(
          icon: Image.asset(
              'assets/images/pokemonlogo.png',
            height: 45,
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageMonsterList()));
          },
        ),
      ),
      body: GridView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int idx) {
          return ComponentMonsterItem(monsterItem: _list[idx], callback: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMonsterDetail(id: _list[idx].id)));
          });
        },
        gridDelegate: _sliverGridDelegate(),
      ),
      backgroundColor: Colors.white,
    );
  }
}
