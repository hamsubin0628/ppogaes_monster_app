import 'package:dio/dio.dart';
import 'package:ppogaes_monster_app/config/config_api.dart';
import 'package:ppogaes_monster_app/model/monster_detail_result.dart';
import 'package:ppogaes_monster_app/model/monster_list_result.dart';

class RepoMonster {
  /**
   * 복수R
   */
  Future<MonsterListResult> getMonsters() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/monster/all'; // 엔드 포인트

    final response = await dio.get(
      _baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return MonsterListResult.fromJson(response.data);
  }

  Future<MonsterDetailResult> getMonster(num id) async{
    Dio dio = Dio();

    String _baseUrl = '$apiUri/monster/detail/{id}';

    final response = await dio.get(
      _baseUrl.replaceAll('{id}', id.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status){
          return status == 200;
        }
      )
    );
    return MonsterDetailResult.fromJson(response.data);
  }
}